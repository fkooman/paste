<?php

declare(strict_types=1);

$yubiKeyList = [
    'vvccccbcgrdj' => [
        'aes_key' => 'e283ef02679764274ae00c0522b8620c',
        'private_id' => 'cda09cd7dbde',
    ],
];

/*
 * Script to verify YubiKey OTPs yourself instead of using their cloud service.
 *
 * You need to "personalize" your YubiKey in order to validate it yourself,
 * i.e. you need to know the AES key and private ID used to "program" it. You
 * can do that with the ykman tool, e.g.:
 *
 * $ ykman otp yubiotp 1 -S -g -G
 * Using YubiKey serial as public ID: vvccccbcgrdj
 * Using a randomly generated private ID: cda09cd7dbde
 * Using a randomly generated secret key: e283ef02679764274ae00c0522b8620c
 * Program a YubiOTP credential in slot 1? [y/N]: y
 *
 * Use the output of this tool to modify the `yubiKeyList` above so the OTPs
 * can be verified.
 *
 * Then run this script:
 * 
 * $ php decrypt_yubikey.php vvccccbcgrdjkrdnlfkgtkklridbrdjjtftrgutfjvig
 * Use Ctr: 2
 * Session Counter: 0
 * Timestamp: 5622789
 * OK!
 * 
 * @see https://developers.yubico.com/OTP/OTPs_Explained.html
 * @see https://github.com/Yubico/yubico-c/blob/master/ykcrc.c (CRC)
 * @see https://github.com/Yubico/yubico-c/blob/master/tests/selftest.c
 * (CRC test vectors)
 */
try {
    if ($argc < 2) {
        throw new Exception('no OTP provided');
    }
    $otpCode = $argv[1];
    if (44 !== strlen($otpCode)) {
        throw new Exception('OTP must be 44 "modhex" chars long');
    }

    $yubiKeyId = substr($otpCode, 0, 12);
    if (!array_key_exists($yubiKeyId, $yubiKeyList)) {
        throw new Exception(sprintf('unknown YubiKey "%s"', $yubiKeyId));
    }
    $yubiKeyInfo = $yubiKeyList[$yubiKeyId];

    // convert modhex to hex
    $otpCode = strtr($otpCode, 'cbdefghijklnrtuv', '0123456789abcdef');
    // convert to binary
    $binaryOtpCode = hex2bin($otpCode);
    // extract the last 16 bytes which is the encrypted AES block
    $cipherText = substr($binaryOtpCode, 6, 16);

    // decrypt
    $plainText = openssl_decrypt(
        $cipherText,
        'aes-128-ecb',
        hex2bin($yubiKeyInfo['aes_key']),
        OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING
    );
    if (false === $plainText) {
        throw new Exception('unable to decrypt OTP');
    }
    if (16 !== strlen($plainText)) {
        throw new Exception('unexpected plain text length');
    }

    // verify the CRC
    if (0xf0b8 !== yubikey_crc16($plainText)) {
        throw new Exception('the CRC is not valid (invalid "checksum")');
    }

    // make sure the first 6 bytes of the decrypted text is the private ID
    if (!hash_equals(hex2bin($yubiKeyInfo['private_id']), substr($plainText, 0, 6))) {
        throw new Exception('unexpected private ID');
    }

    // use counter (total number of times the YubiKey was plugged in and used,
    // with possible rollover if sessionCtr ever reached >255, i.e. if it was
    // pressed more than 255 times while plugged in once)
    $useCtr = bytes_to_short(substr($plainText, 6, 2));
    echo 'Use Ctr: ' . $useCtr . PHP_EOL;

    // times the button was pressed since YubiKey was plugged in, MAY roll over
    $sessionCtr = ord(substr($plainText, 11, 1));
    echo 'Session Counter: ' . $sessionCtr . PHP_EOL;

    // timestamp, increased by ~8 every second (8 Hz). Initialized with a
    // random number after plug-in. Also loops around, on average every ~24
    // days
    echo 'Timestamp: ' . bytes_to_long(substr($plainText, 8, 3) . "\x00") . PHP_EOL;

    // create 1 number that is always incremented after use / being plugged in
    $counterValue = $useCtr * 256 + $sessionCtr;

    // very rudimentary token replay protection
    // WARNING: this is NOT safe, has race condition, and writes in the temp
    // dir, but good enough for simple testing
    $replayCheckFile = sys_get_temp_dir() . '/' . $yubiKeyId . '.counter';
    if (false !== $replayCounter = @file_get_contents($replayCheckFile)) {
        if ($counterValue <= (int) $replayCounter) {
            // this also prevents old values that were never used, e.g. stored
            // somewhere, from being used *after* a newer one, which is good!
            throw new Exception('token replay!');
        }
    }
    if (false === file_put_contents($replayCheckFile, (string) $counterValue)) {
        throw new Exception('unable to write file to prevent OTP replay');
    }

    echo "OK!" . PHP_EOL;
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . PHP_EOL;
    exit(1);
}

function yubikey_crc16(string $buf): int
{
    $m_crc = 0xffff;
    for ($l = 0; $l < strlen($buf); $l++) {
        $m_crc ^= ord($buf[$l]) & 0xff;
        for ($i = 0; $i < 8; $i++) {
            $j = $m_crc & 1;
            $m_crc >>= 1;
            if ($j) {
                $m_crc ^= 0x8408;
            }
        }
    }

    return $m_crc;
}

function bytes_to_short(string $in): int
{
    $u = unpack('vshort', $in);
    if (array_key_exists('short', $u) && is_int($u['short'])) {
        return $u['short'];
    }

    throw new Exception('unable to convert bytes to short');
}

function bytes_to_long(string $in): int
{
    $u = unpack('Vlong', $in);
    if (array_key_exists('long', $u) && is_int($u['long'])) {
        return $u['long'];
    }

    throw new Exception('unable to convert bytes to long');
}
