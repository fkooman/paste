# Separate Routing Table for WireGuard on Linux with Split Tunnel VPNs

Download a "split tunnel" WireGuard configuration from the (edu)VPN portal, 
e.g. `split.conf`. This is one option to deal with overlap in IP addresses, 
i.e. the VPN server's public IP address is contained within the prefix that is 
routed over the VPN. Without any countermeasures a routing loop would occur.

```bash
$ nmcli con import type wireguard file split.conf
$ nmcli con modify split wireguard.fwmark 0xca6c
$ nmcli con modify split ipv4.route-table 51820
$ nmcli con modify split ipv4.routing-rules "priority 5 not fwmark 51820 table 51820"
$ nmcli con modify split ipv6.route-table 51820
$ nmcli con modify split ipv6.routing-rules "priority 5 not fwmark 51820 table 51820"
$ nmcli con down split
$ nmcli con up split
```

This imports the WireGuard configuration file and updates some settings that 
will make the connection use its own routing table, makes sure the VPN tunnel
packets are marked using `fwmark` with value `0xca6c` (which is 51820 in 
hexadecimal) so they can be matched by the rule. After that it restarts the
connection and all should be working as intended.

More information:

* https://www.wireguard.com/netns/

