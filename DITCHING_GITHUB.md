**VERY DRAFT OUTLINE**

# Ditching GitHub

## What is GitHub

A social media platform.

"The world’s leading AI-powered developer platform."

## What is the problem with GitHub?

There are *many* issues with GitHub:

* Practical
  * Vendor lockin
* Ethical
  * Forces (potential) contributors to create an account at GitHub (Microsoft) 
    and accept the terms
  * Focus on "AI", which would require a complete separate presentation to 
    explain all the issues with this...
* Policical
  * Too big to fail / jail
  * Microsoft collaborates with e.g. ICE
  * Refuses to follow GDPR
  * Not compatible with Tech. Sovereignty
* Legal
  * Uses your source code to train their "AI" systems without permission, many
    documented cases of Copilot violating free software licenses
* Technical
  * *Still* doesn't support IPv6
  * The CI is very much GitHub specific, no way to run it offline on your own
    system

Also: finally fixing cognitive dissonance: using software and services that 
align with your ethics. It is strange to use a completely closed platform for
hosting your free software projects.

## Alternatives

* Codeberg
* SourceHut
* Self hosting

## Feature Gaps

CI of Codeberg is currently the weak point. The CI of SourceHut is kilometers
ahead of GitHub's.

## Migrating

### Codeberg

* Use the UI

### SourceHut

* `git clone --mirror`

## Is it worth it?

Time will tell, but it is nice not to have to be a hypocrite any longer! The CI
of SourceHut itself is already worth it. 

## Conclusion

We moved all server development of the eduVPN project to SourceHut a long time
ago and are now in the process of slowly moving them to Codeberg.

## Sources

* https://ploum.net/2023-02-22-leaving-github.html
* https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html

