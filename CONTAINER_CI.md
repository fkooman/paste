# Custom Container Image for Forgejo Actions

How to create a container image for use by Forgejo Actions?

Create `Containerfile` with the following content, we install all the 
dependencies we need for our software:

```
FROM fedora:latest
RUN dnf -y --refresh upgrade; dnf clean all
RUN dnf -y install make php-xml php-gmp php-intl php-maxminddb php-mbstring php-pecl-pcov scdoc wget golang-go phpunit9 composer git unzip php-pdo php-sodium php-pdo_sqlite; dnf clean all
RUN useradd -m ci-user
USER ci-user
```

Using `podman`, we'll create an image we can push to `codeberg.org`, obviously
update the "tag" to whatever is appropriate for you:

```bash
$ podman build -t codeberg.org/fkooman/ci:latest .
```

Once this is finished, you can list it:

```bash
$ podman images
codeberg.org/fkooman/ci            latest      1306818846df  3 minutes ago  807 MB
registry.fedoraproject.org/fedora  latest      adc0a2cc13f6  3 hours ago    164 MB
```

Now you want to "push" it to `codeberg.org`, you first need to login:

```bash
$ podman login codeberg.org
```

It will ask for your username and password, as a password make sure you 
generate a token [here](https://codeberg.org/user/settings/applications) and
ONLY specify the "Package (Read and write)" permission.

Now you can push the image:

```bash
$ podman push codeberg.org/fkooman/ci:latest
```

This will take a while, unless you have a really fast Internet connection.

# Updating Image

It is not exactly clear (to me) how to update an image when reusing the same 
tag, e.g. `:latest`. If you locally create a new one, and push it, the pushing 
seems to work and is also reflected in the web UI, but `forgejo-runner` does 
NOT pick it up for some reason. Two things _may_ be at play here:

1. The `forgejo-runner` keeps a cached version of the container, and as the tag
   wasn't updated it is assumed it has the latest version;
2. The `podman push` does not actually "replace" the CI version with the same
   tag?
   
For (1) we can probably be resolved with the `force_pull` option, but I don't
know if that _really_ "pulls" the image always, or only when it has been 
modified. The latter would be great.

# Using in CI

Now, in your CI script, you can set `image` to `codeberg.org/fkooman/ci:latest` 
and start from there, e.g.:

```
on: [push]
jobs:
    tests:
        runs-on: docker
        container:
            image: codeberg.org/fkooman/ci:latest
        steps:
            - name: Clone Repository
              run: |
                git clone --depth 1 -b ${{ github.ref_name }} ${{ github.server_url }}/${{ github.repository }} .
            - name: Install Dependencies
              run: |
                composer update                
            - name: Run Unit Tests
              run: |
                # use `unshare` to not allow any network access
                unshare -U -n phpunit9
```

# References

* https://forgejo.org/docs/latest/user/packages/container/
* https://forgejo.org/docs/latest/admin/runner-installation/
