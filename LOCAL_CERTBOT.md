# Local Certbot

Challenge: if you run a CDN with multiple nodes hosting the same domains, you
need to have a key/cert on each of them. When using Let's Encrypt (or other 
ACME-compatible) service, you need to be able to prove ownership using 
[HTTP-01](https://letsencrypt.org/docs/challenge-types/#http-01-challenge).

So the plan is to run `certbot` in "manual" mode on a workstation that has SSH 
access to the CDN nodes and use some 
[hooks](https://eff-certbot.readthedocs.io/en/stable/using.html#pre-and-post-validation-hooks).

The scripts below should be put somewhere on disk, e.g. in `${HOME}/acme`.

**NOTE**: you do NOT need to run this as `root`, nor use `sudo`!

`run.sh`:

```bash
#!/bin/sh

certbot \
    certonly \
    --manual \
    --preferred-challenges=http \
    --manual-auth-hook "${PWD}/authenticator.sh" \
    --manual-cleanup-hook "${PWD}/cleanup.sh" \
    --config-dir "${PWD}/config" \
    --work-dir "${PWD}/work" \
    --logs-dir "${PWD}/logs" \
    -d vpn.tuxed.net
```

`authenticator.sh`:

```bash
#!/bin/sh
echo "${CERTBOT_VALIDATION}" > "${CERTBOT_TOKEN}"
scp "${CERTBOT_TOKEN}" "vpn.tuxed.net:/var/www/html/.well-known/acme-challenge/${CERTBOT_TOKEN}"
```

`cleanup.sh`:

```bash
#!/bin/sh
rm "${CERTBOT_TOKEN}"
ssh vpn.tuxed.net rm "/var/www/html/.well-known/acme-challenge/${CERTBOT_TOKEN}"
```

This shows it only for 1 domain with one server, but it _does_ work!
