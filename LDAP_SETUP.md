# LDAP Server Setup

It is not easy to find a working simple LDAP server to play with, perhaps 
except the one provided by OpenBSD. The concepts are not easy if you are not 
familiar with LDAP, nor the (initial) configuration and managing LDAP.

There are numerous full features servers that make it somewhat easier to get 
started, but they are generally very bloated and overkill for small 
organizations, let alone simple test setups.

On Linux we have OpenLDAP which _should_ be easy, but is not really. So this 
document will provide instructions to set up a relatively minimal usable LDAP 
server that is as secure as possible.

Our aim is to set up an LDAP server you can expose directly to the Internet 
without having to worry much about it.

* [Installation](#installation)
* [Configuration](#configuration)
* [OUs](#ous)
* [Users](#users)
* [Dynamic Group Membership](#dynamic-group-membership)
* [LDAP Schemas](#ldap-schemas)
* [LDAPS](#ldaps)
* [Modern Password Hashes](#modern-password-hashes)

# Installation

We only tested on Debian 12, make sure you have a relatively clean Debian 12
installation to follow along.

Install the required packages:

```bash
$ sudo apt install slapd ldap-utils
```

# Configuration

When you install `slapd` Debian will ask you to configure LDAP already (a 
little bit). It will ask you to specify a domain name. We'll use `home.arpa` as
the example below. You're also asked to provide an "admin" password.

If you want to restart the configuration:

```bash
$ sudo dpkg-reconfigure -plow slapd
```

Your LDAP's base will be `dc=home,dc=arpa`. The `dn` of your `admin` will be 
`cn=admin,dc=home,dc=arpa`. This information is important when populating the
LDAP later.

# OUs

Just throwing people and groups in the base directory, i.e. `dc=home,dc=arpa`, 
may not be ideal, that's why we'll create two organizational units (OUs).

## Adding OUs

We'll create two OUs, one for users:

```bash
$ ldapadd -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: ou=people,dc=home,dc=arpa
objectClass: organizationalUnit
ou: People
EOF
``` 

And one for groups:

```bash
$ ldapadd -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: ou=groups,dc=home,dc=arpa
objectClass: organizationalUnit
ou: Groups
EOF
```

# Users

## Adding Users

Now on to the good stuff. Adding users, that is what it is all about. All 
instructions you can find online omit this part. Just when it gets good.

For example:

```bash
$ ldapadd -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: uid=fkooman,ou=people,dc=home,dc=arpa
uid: fkooman
gn: François
cn: F. Kooman
displayName: François Kooman
sn: Kooman
objectClass: person
objectClass: inetOrgPerson
EOF
```

## Setting Password

Initially we'll have the `cn=admin,dc=home,dc=arpa` account set the password, 
after which the user can update the password themselves later.

```bash
$ ldappasswd -x -D 'cn=admin,dc=home,dc=arpa' -W uid=fkooman,ou=people,dc=home,dc=arpa -S
```

First you'll provide the (new) user's password twice (to make sure they match), 
and then the admin password.

## Updating Password

The admin can set the password as seen above, but the user can also update 
their own password:

```bash
$ ldappasswd -x -D 'uid=fkooman,ou=people,dc=home,dc=arpa' -W 'uid=fkooman,ou=people,dc=home,dc=arpa' -S
```

Same rules apply, but now instead of the admin password, at the last prompt, 
the user provides _their_ old password.

## Adding Attribute (Value)

We'll add the `mail` attribute, with the value `fkooman@home.arpa`:

```bash
$ ldapmodify -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: uid=fkooman,ou=people,dc=home,dc=arpa
changetype: modify
add: mail
mail: fkooman@home.arpa
EOF
```

## Deleting Users

```bash
$ ldapdelete -x -D 'cn=admin,dc=home,dc=arpa' -W 'uid=fkooman,ou=people,dc=home,dc=arpa'
```

## Listing Users

Anonymous bind search for user with `uid=fkooman`:

```bash
$ ldapsearch -LLL -x -b 'ou=people,dc=home,dc=arpa' uid=fkooman
dn: uid=fkooman,ou=people,dc=home,dc=arpa
uid: fkooman
givenName:: RnJhbsOnb2lz
cn: F. Kooman
displayName:: RnJhbsOnb2lzIEtvb21hbg==
sn: Kooman
objectClass: person
objectClass: inetOrgPerson
mail: fkooman@home.arpa
```

Note that some values here are Base64 encoded, i.e. `givenName` and 
`displayName` because they are UTF-8 encoded. The double colon, i.e. `::` 
indicates that a value is Base64 encoded.

If you authenticate as yourself, you can also see the `userPassword` attribute 
value, also Base64 encoded:

```bash
$ ldapsearch -LLL -x -D 'uid=fkooman,ou=people,dc=home,dc=arpa' -W -b 'ou=people,dc=home,dc=arpa' uid=fkooman
```

# Dynamic Group Membership

This section is **OPTIONAL**.

If you add users to groups, it would be nice if the group membership was not
only visible when querying the group, but _also_ when querying the user. The 
groups use the `member` attribute for members, the users use `memberOf` to 
indicate the group membership.

**NOTE**: according to the OpenLDAP document, i.e. `slapo-memberof(5)` this
overlay is deprecated and will be replaced by `slapo-dynlist(5)` in the future.
Unfortunately it seems that `dynlists` are a bit more complex, or I just don't
quite get it yet. In theory it is simple, when requesting `memberOf` of a user,
an internal "search" is performed to list the membership for that user, but how
to configure this in the most simple way still eludes me.

First we need to add the `memberof` module:

```bash
# ldapmodify -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: memberof
EOF
```

Enable the overlay:

```bash
# ldapadd -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: olcOverlay=memberof,olcDatabase={1}mdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcMemberOf
olcOverlay: memberof
olcMemberOfRefInt: TRUE
EOF
```

**NOTE**: this above entry I found somewhere online, not in any official 
documentation.

Create a group with our user:

```bash
$ ldapadd -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: cn=employees,ou=groups,dc=home,dc=arpa
objectClass: groupOfNames
cn: Employees
member: uid=fkooman,ou=people,dc=home,dc=arpa
EOF
```

List the employees "group":

```bash
$ ldapsearch -LLL -x -b cn=employees,ou=groups,dc=home,dc=arpa
dn: cn=employees,ou=groups,dc=home,dc=arpa
objectClass: groupOfNames
cn: Employees
member: uid=fkooman,ou=people,dc=home,dc=arpa
```

Now, request the `memberOf` attribute under the user's entry:

```bash
$ ldapsearch -LLL -x -b ou=people,dc=home,dc=arpa uid=fkooman memberOf
dn: uid=fkooman,ou=people,dc=home,dc=arpa
memberOf: cn=employees,ou=groups,dc=home,dc=arpa
```

# LDAP Schemas

This section is **OPTIONAL**.

If you want to add a schema that is not part of the default installations, that
is really easy. We'll use the `eduPerson` schema as an example, which is very
common in R&E.

Conveniently, the OpenLDAP schema is provided 
[here](https://github.com/REFEDS/eduperson/tree/master/schema/openldap).

You can download the two files and add them to `/etc/ldap/schema` where also 
all the other schema reside. Technically only the `ldif` file is needed, but
why not follow the convention.

In order to tell `slapd` about this schema, you have to add it to the 
configuration.

```bash
# ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/eduperson.ldif
```

To add an `eduPerson` attribute to the user, you can do the following:

```bash
$ ldapmodify -x -D 'cn=admin,dc=home,dc=arpa' -W << EOF
dn: uid=fkooman,ou=people,dc=home,dc=arpa
changetype: modify
add: objectClass
objectClass: eduPerson
-
add: eduPersonEntitlement
eduPersonEntitlement: http://eduvpn.org/role/admin
EOF
```

# LDAPS

This section is **OPTIONAL** if you do NOT want to put your LDAP server on the 
Internet.

In this section we'll enable TLS and require client certificate authentication
as well (mutual TLS).

If you are not comfortable generating your own CA, server and client 
certificate, you can use [vpn-ca](https://git.sr.ht/~fkooman/vpn-ca) for this
purpose.

```bash
$ vpn-ca -init-ca -domain-constraint .home.arpa -name "Home CA"
$ vpn-ca -server -name ldap.home.arpa
$ vpn-ca -client -name ldap-client
```

Configuring OpenLDAP to use it, is not that difficult, put the files in the 
right place and make sure the `openldap` group can read them. 

Make sure the group `openldap` has access to `/etc/ssl/private` and that the 
files themselves can be read by `openldap`:

```bash
$ sudo chown root:openldap /etc/ssl/private /etc/ssl/private/ldap.home.arpa.key
$ sudo chmod 0750 /etc/ssl/private
$ sudo chmod 0640 /etc/ssl/private/ldap.home.arpa.key
$ sudo chmod 0644 /etc/ssl/certs/ldap.home.arpa.crt
$ sudo chmod 0644 /etc/ssl/certs/ca.crt
```

Now configure the CA, certificate and key and enable requiring a client 
certificate:

```bash
# ldapmodify -H ldapi:/// -Y EXTERNAL << EOF
dn: cn=config
changetype: modify
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ssl/certs/ldap.home.arpa.crt
-
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ssl/private/ldap.home.arpa.key
-
replace: olcTLSVerifyClient
olcTLSVerifyClient: demand
-
replace: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ssl/certs/ca.crt
EOF
```

If you are deploying on a platform that uses OpenSSL with OpenLDAP, and not 
GnuTLS, you can require at least TLSv1.3 which would be neat. On Debian 12 this
does not work as GnuTLS is used:

```bash
# ldapmodify -H ldapi:/// -Y EXTERNAL << EOF
dn: cn=config
changetype: modify
replace: olcTLSProtocolMin
olcTLSProtocolMin: 3.4
EOF
```

Enable TLS, over port `636` and restart `slapd`. Modify `/etc/default/slapd` 
and set `SLAPD_SERVICES` to `"ldap:/// ldapi:/// ldaps:///"`.

Restart `slapd`:

```bash
$ sudo systemctl restart slapd
```

You can check whether it works:

```bash
$ LDAPTLS_CACERT=ca.crt LDAPTLS_CERT=ldap-client.crt LDAPTLS_KEY=ldap-client.key ldapwhoami -H ldaps://ldap.home.arpa -x
anonymous
```

Here `ca.crt`, `ldap-client.crt` and `ldap-client.key` are the CA certificate 
and certificate and key you created using `vpn-ca` above.

## Connecting to LDAPS with PHP

We ran into some [difficulty](https://github.com/php/php-src/issues/12081) 
making connections work from PHP. The example below shows how to perform 
`ldap_whoami` from PHP talking to a server with a self-signed CA and client
certificate authentication. Note, this only works in PHP >= 7.1.

```php
<?php
$ldapOptions = [
    LDAP_OPT_X_TLS_CACERTFILE => __DIR__.'/ca.crt',
    LDAP_OPT_X_TLS_CERTFILE => __DIR__.'/ldap-client.crt',
    LDAP_OPT_X_TLS_KEYFILE => __DIR__.'/ldap-client.key',
    LDAP_OPT_PROTOCOL_VERSION => 3,
    LDAP_OPT_REFERRALS => 0,
];
foreach($ldapOptions as $k => $v) {
    ldap_set_option(null, $k, $v);
}
$ldapResource = ldap_connect('ldaps://ldap.home.arpa:636');
ldap_bind($ldapResource, null, null);
if('' === $whoAmI = ldap_exop_whoami($ldapResource)) {
    $whoAmI = 'anonymous';
}
echo sprintf("ldap_exop_whoami: %s\n", $whoAmI);
```

# Modern Password Hashes

This section is **OPTIONAL**.

In order to enable modern password hashes, we can load the `argon2` module:

```bash
# ldapmodify -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: argon2
EOF
```

Then set the password hashing algorithm to `{ARGON2}`.

```bash
# ldapmodify -Q -Y EXTERNAL -H ldapi:/// << EOF
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
add: olcPasswordHash
olcPasswordHash: {ARGON2}
EOF
```

Now you'll want to update the password of the `admin` account:

To generate a new hash for the admin, use:

```bash
$ slappasswd -h "{ARGON2}" -o module-load=argon2
```

Make sure the output is a long string that starts with `{ARGON2}`.

In the example below replace the `olcRootPW` line with the hash you 
generated above:

```bash
# ldapmodify -Q -Y EXTERNAL -H ldapi:/// << 'EOF'
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {ARGON2}xxxxxx
EOF
```

This should update the password. You can see whether it worked:

```bash
# ldapsearch -LLLQ -Y EXTERNAL -H ldapi:/// -b olcDatabase={1}mdb,cn=config olcRootPW
dn: olcDatabase={1}mdb,cn=config
olcRootPW: {ARGON2}xxxxxx
```

Now try to authenticate:

```bash
$ ldapwhoami -x -D cn=admin,dc=home,dc=arpa -W
Enter LDAP Password: 
dn:cn=admin,dc=home,dc=arpa
```

All good! 

**NOTE**: if you added users and set their password, they'll still be using 
the _old_ hash. Next time you'll set the user's password, or the user updates 
it themselves it will be in the Argon2 format.

**NOTE**: we've submitted an (accepted) 
[patch](https://git.openldap.org/openldap/openldap/-/merge_requests/643) to 
make the password hash format `argon2id` instead of `argon2i` when using 
`libargon2` (on Debian). It will most likely be in the next Debian version.

# References

* [OpenLDAP memberOf overlay](https://tylersguides.com/guides/openldap-memberof-overlay/)
* [eduPerson Specification](https://refeds.org/specifications/eduperson)
* [Debian OpenLDAP Setup](https://wiki.debian.org/LDAP/OpenLDAPSetup)
