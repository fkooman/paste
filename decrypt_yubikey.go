package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"os"
)

type YubiInfo struct {
	PublicId  string
	PrivateId string
	AesKey    string
}

// @see https://github.com/Yubico/yubico-c/blob/master/ykcrc.c
func yubiCrc(buf []byte) uint16 {
	var m_crc uint16 = 0xffff
	for l := 0; l < len(buf); l++ {
		var j uint16
		m_crc ^= uint16(buf[l] & 0xff)
		for i := 0; i < 8; i++ {
			j = m_crc & 1
			m_crc >>= 1
			if j != 0 {
				m_crc ^= 0x8408
			}
		}
	}

	return m_crc
}

// translate 'cbdefghijklnrtuv' => '0123456789abcdef'
// there really must be a better way
// fork Go's hex.go with updated alphabet?
func modHexToHex(src []byte) []byte {
	dst := []byte{}
	m := map[string]string{
		"c": "0",
		"b": "1",
		"d": "2",
		"e": "3",
		"f": "4",
		"g": "5",
		"h": "6",
		"i": "7",
		"j": "8",
		"k": "9",
		"l": "a",
		"n": "b",
		"r": "c",
		"t": "d",
		"u": "e",
		"v": "f",
	}

	for _, v := range src {
		v1, ok := m[string(v)]
		if !ok {
			continue
		}
		dst = append(dst, []byte(v1)...)
	}

	return dst
}

func main() {
	// as provided by YubiKey when pressing the button
	//	otpCode := "vvccccbcgrdjfirlgcuklhbdfhuihdtjitekiugegier"
	otpCode := os.Args[1]

	if len(otpCode) != 44 {
		log.Fatal("modhex otp code MUST be 44 chars long")
	}

	yubiInfo := YubiInfo{
		PublicId:  "vvccccbcgrdj",
		PrivateId: "e42c4a17ee3a",
		AesKey:    "91c340c7767497b32d39615ecd8d0a6d",
	}

	// make sure the first 12 chars (6 bytes) of the OTP match the registered
	// public ID
	if otpCode[0:12] != yubiInfo.PublicId {
		log.Fatal("we do not know this YubiKey")
	}

	// modhex to hex conversion
	hexOtpCode := modHexToHex([]byte(otpCode))
	if len(hexOtpCode) != 44 {
		log.Fatal("hex otp code MUST still be 44 chars long")
	}

	// the last 16 bytes (32 hex chars) is the AES-128-CBC encrypted ciphertext
	cipherBytes := make([]byte, 16)
	hex.Decode(cipherBytes, []byte(hexOtpCode[12:]))

	// decrypt cipherBytes
	aesKey, _ := hex.DecodeString(yubiInfo.AesKey)

	block, err := aes.NewCipher(aesKey)
	if err != nil {
		log.Fatal(err)
	}
	if len(cipherBytes) != aes.BlockSize {
		log.Fatal("ciphertext must be exactly 16 bytes")
	}

	// the IV is 16 bytes \x00 which I guess is fine as we only ever encrypt
	// 16 bytes with this key?!
	mode := cipher.NewCBCDecrypter(block, make([]byte, 16))
	// decrypt "in place"
	mode.CryptBlocks(cipherBytes, cipherBytes)
	// Cipher Bytes are hex encoded: e42c4a17ee3a010061eb61000f26e724

	// verify the CRC, don't ask me how this works...
	if yubiCrc(cipherBytes) != 61624 {
		log.Fatal("CRC does not match")
	}

	// make sure the Private ID is what we expect it to be
	if yubiInfo.PrivateId != hex.EncodeToString(cipherBytes[0:6]) {
		log.Fatal("private ID does NOT match...")
	}

	// use counter (total number of times the YubiKey was plugged in and used,
	// with possible rollover if sessionCtr ever reached >255, i.e. if it was
	// pressed more than 255 times while plugged in once)
	useCtr := binary.LittleEndian.Uint16(cipherBytes[6:8])
	fmt.Printf("Use Counter: %d\n", useCtr)

	// times the button was pressed since YubiKey was plugged in, MAY roll over
	sessionCtr := uint8(cipherBytes[11])
	fmt.Printf("Session Counter: %d\n", sessionCtr)

	// timestamp, increased by ~8 every second (8 Hz). Initialized with a
	// random number after plug-in. Also loops around, on average every ~24
	// days
	timestamp := binary.LittleEndian.Uint32(append(cipherBytes[8:11], []byte{0x00}...))
	fmt.Printf("Timestamp: %d\n", timestamp)

	// create 1 number that is always incremented after use / being plugged in
	counterValue := int(useCtr)*256 + int(sessionCtr)
	fmt.Printf("Counter Value: %d\n", counterValue)
}
